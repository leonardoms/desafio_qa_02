package org.br.curso.tests.service;

import static com.br.inmetrics.frm.bdd.Gherkin.executeScenario_;

import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.br.inmetrics.frm.base.TestBase;
import com.br.inmetrics.frm.controllers.EmptyController;
import com.br.inmetrics.frm.testng.TestConfig;


public class TestServiceGherkin extends TestBase {

	@TestConfig(controllerType = EmptyController.class)
	public class TestSolicitarCartaoAdicionalGherkin extends TestBase{
		
		@Test(groups = {"POST"}, priority = 1, testName = "Validar response de user")
		public void validarReponseDeUser() {
	
			try {
				executeScenario_("Service", "Validar response de user");
			} catch (Exception e) {
				Assert.fail("Test error.", e);
			}
		}
		
		
		@Test(groups = {"GET"}, priority = 1, testName = "Validar itens que estao com o completed true")
		public void validarItensQueEstaoComCompletedTrue() {
	
			try {
				executeScenario_("Service", "Validar itens que estao com o completed true");
			} catch (Exception e) {
				Assert.fail("Test error.", e);
			}
		}
		
		@BeforeMethod(alwaysRun=true)
		public void setup(final Method method, final ITestContext context) {
	
			super.setup(method, context);
	
		}
	
		@AfterMethod
		public void teardown(final Method method, final ITestContext context) {
	
			super.teardown(method, context);
		}
	
	}
	
}
