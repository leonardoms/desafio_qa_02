package org.br.curso.steps.service;

import static com.br.inmetrics.frm.base.DefaultBaseController.getPage_;

import org.br.curso.actions.service.ServiceActions;

import com.br.inmetrics.frm.bdd.Step;

public class ServiceSteps {
	
	ServiceActions actions = getPage_(ServiceActions.class);
	
	/*
	 * Scenario: Validar response de user
	 */
	@Step("Dado que eu acesse a API de User")
	public void dado_que_eu_acesse_a_api_de_user() {
		actions.setUri("http://fakerestapi.azurewebsites.net/api/Users");
	}
	
	@Step("Quando obter o user")
	public void quando_obter_o_user() {
		actions.postUser();
	}
	
	@Step("Entao o user deverá ser valido")
	public void entao_o_user_devera_ser_valido() {
		actions.validarUser();
	}
	
	
	/*
	 * Scenario: Validar itens qeu estão com o completed true
	 */
	@Step("Dado que eu acesse a API com todos os usuarios")
	public void dado_que_eu_acesse_a_api_com_todos_os_usuarios() {
		actions.setUri("https://jsonplaceholder.typicode.com/todos");
	}
	
	@Step("Quando obter todos os usuarios")
	public void quando_obter_todos_os_usuarios() {
		actions.getUsers();
	}
	
	@Step("Entao os usuarios deverao ser validos com o status completed true")
	public void entao_os_usuarios_deverao_ser_validos_com_o_status_completed_true() {
		actions.validarUser();
	}

}
