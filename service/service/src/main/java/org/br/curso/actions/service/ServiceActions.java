package org.br.curso.actions.service;

import static io.restassured.RestAssured.given;

import com.br.inmetrics.frm.base.PageBase;
import com.br.inmetrics.frm.helpers.LoggerHelper;

import com.google.common.collect.ImmutableMap;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;



public class ServiceActions extends PageBase{
	
	Response response;
	RequestSpecification request;

	LoggerHelper logger = new LoggerHelper(ServiceActions.class);
	
	public void setUri(String uri) {
		RestAssured.baseURI = uri;
	}
	
	public void postUser() {
		
		request = given()
		.header("Accept", "")
		.contentType(ContentType.JSON.withCharset("UTF-8"))
		.body(ImmutableMap.builder().put("ID", "1")
									.put("UserName", "Leonardo")
									.put("Password", "1111").build())
		.log().all();
	
		response = request
						.when()
						.post("/1");
		
		
	}
	
	public void validarUser() {
		logger.info("Reponse" + response.asString());
		response.then().statusCode(200);
	}
	
	public void getUsers() {
		
		request = given()
		.header("Accept", "")
		.contentType(ContentType.JSON.withCharset("UTF-8"))
		.param("completed", "true")
		.log().all();
		
		response = request
						.when()
						.get();
	}
}
