package org.br.curso.features.service;

import static com.br.inmetrics.frm.bdd.Gherkin.*;

import java.util.concurrent.ExecutionException;

import com.br.inmetrics.frm.bdd.Feature;
import com.br.inmetrics.frm.bdd.Scenario;

@Feature("Service")
public class ServiceFeatures {
	

	@SuppressWarnings("static-access")
	@Scenario("Validar response de user")
	public void validarResponseDeUser() throws ExecutionException {
		
		given_("Dado que eu acesse a API de User").
		when_("Quando obter o user").
		then_("Entao o user deverá ser valido").
		execute_();
	}
	
	@SuppressWarnings("static-access")
	@Scenario("Validar itens que estão com o completed true")
	public void validarItensQueEstaoComOCompletedTrue() throws ExecutionException {
		
		given_("Dado que eu acesse a API com todos os usuarios").
		when_("Quando obter todos os usuarios").
		then_("Entao os usuarios deverao ser validos com o status completed true").
		execute_();
	}
	
}
