package com.br.exercicio.testes.web;
import static com.br.inmetrics.frm.bdd.Gherkin.executeScenario_;
import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.br.inmetrics.frm.base.TestBase;
import com.br.inmetrics.frm.controllers.WebController;
import com.br.inmetrics.frm.testng.DataTableConfig;
import com.br.inmetrics.frm.testng.TestConfig;

@TestConfig(controllerType = WebController.class)
public class TestLoginGherkin extends TestBase{
	
	@DataTableConfig(ct = 1)
	@Test(groups = {"Login"}, priority = 1, testName = "CT001 - Acessar com usuario administrador")
	public void CT001AcessarUsuarioAdm () {		
		try {
			executeScenario_("Login","Acessar com usuario administrador");
		} catch (Exception e) {
			Assert.fail("TestError.", e); 
		} 
	}
	
	@DataTableConfig(ct = 2)
	@Test(groups = {"Login"}, priority = 1, testName = "CT002 - Acessar com e-mail invalido")
	public void CT002AcessarComEmailInvalido () {		
		try {
			executeScenario_("Login","Acessar com e-mail invalido");
		} catch (Exception e) {
			Assert.fail("TestError.", e); 
		} 
	}
	
	
	@BeforeMethod(alwaysRun = true)
	public void setup(final Method method, final ITestContext context) {
		super.setup(method, context);
	}
	
	@AfterMethod
	public void teardown(final Method method, final ITestContext context) {
		super.teardown(method, context);
	}

}
