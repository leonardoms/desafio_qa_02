package com.br.exercicio.testes.web;

import static com.br.inmetrics.frm.bdd.Gherkin.executeScenario_;

import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.br.inmetrics.frm.base.TestBase;
import com.br.inmetrics.frm.controllers.WebController;
import com.br.inmetrics.frm.testng.DataTableConfig;
import com.br.inmetrics.frm.testng.TestConfig;

@TestConfig(controllerType = WebController.class)
public class TestCadastroGherkin extends TestBase {

	@DataTableConfig(ct = 3)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT003 - Realizar cadastro de um usuario no site")
	public void CT003_RealizarCadastroDeUsuarioNoSite() {

		try {
			executeScenario_("Cadastro", "CT003 - Realizar cadastro de um usuario no site");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}	
	
	@DataTableConfig(ct = 4)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT004 - Realizar inclusao de email ja existente")
	public void CT004_RealizarInclusaoDeEmailExistenteNoSite() {

		try {
			executeScenario_("Cadastro", "CT004 - Realizar inclusao de email ja existente");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}		
	
	@DataTableConfig(ct = 5)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT005 - Realizar edicao de um usuario no site")	
	public void CT005_RealizarEdicaoDeUsuarioNoSite() {

		try {
			executeScenario_("Cadastro", "CT005 - Realizar edicao de um usuario no site");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}
	
	
	@DataTableConfig(ct = 6)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT006 - Realizar exclusao de um usuario no site")	
	public void CT006_RealizarExclusaoDeUsuarioNoSite() {

		try {
			executeScenario_("Cadastro", "CT006 - Realizar exclusao de um usuario no site");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}
	
	
	@DataTableConfig(ct = 7)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT007 - Realizar pesquisa de usuario existente")
	public void CT007_RealizarPesquisaDeUsuarioExistenteNoSite() {

		try {
			executeScenario_("Cadastro", "CT007 - Realizar pesquisa de usuario existente");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}
	
	
	@DataTableConfig(ct = 8)
	@Test(groups = {"Cadastro"}, priority = 1, testName = "CT008 - Realizar pesquisa de usuario inexistente")
	public void CT008_RealizarPesquisaDeUsuarioInexistenteNoSite() {

		try {
			executeScenario_("Cadastro", "CT008 - Realizar pesquisa de usuario inexistente");
		} catch (Exception e) {
			Assert.fail("Test error.", e);
		}
	}	
	
	
	@BeforeMethod(alwaysRun = true)
	public void setup(final Method method, final ITestContext context) {

		super.setup(method, context);
	}

	@AfterMethod
	public void teardown(final Method method, final ITestContext context) {

		super.teardown(method, context);
	}
}
