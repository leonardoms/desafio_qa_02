package com.br.exercicio.feature.web;


import com.br.inmetrics.frm.bdd.Feature;
import com.br.inmetrics.frm.bdd.Scenario;
import static com.br.inmetrics.frm.bdd.Gherkin.*;
import java.util.concurrent.ExecutionException;


//ESCREVENDO CENARIOS
@Feature("Cadastro")
public class CadastroFeature {

	/*
	 *  REALIZANDO O CADASTRO DE UM NOVO USUARIO NO SITE 
	 */
	
	@SuppressWarnings("static-access")
	@Scenario("CT003 - Realizar cadastro de um usuario no site")
	public void cadastroUsuario() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("preencho as informacoes solicitadas da pagina")
		.then_("sistema armazena o novo cadastro")
		.execute_();
	}
	
	/*
	 *  REALIZANDO INCLUSAO DE UM USUARIO COM O MESMO EMAIL NO SITE
	 */
	
	@SuppressWarnings("static-access")
	@Scenario("CT004 - Realizar inclusao de email ja existente")
	public void cadastrarMesmoEmailNoSite() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("preencho as informacoes solicitadas da pagina")
		.then_("sistema n�o permite inclusao de um email existente")
		.execute_();
	}
	
	/*
	 *  REALIZANDO A EDI��O DE UM NOVO USUARIO NO SITE 
	 */	
	
	@SuppressWarnings("static-access")
	@Scenario("CT005 - Realizar edicao de um usuario no site")
	public void editarUsuario() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("seleciono um usuario da lista e edito suas informa��es")
		.then_("informa��es atualizadas")
		.execute_();
	}
	
	/*
	 *  REALIZANDO A EXCLUSAO DE UM NOVO USUARIO NO SITE 
	 */		
	
	@SuppressWarnings("static-access")
	@Scenario("CT006 - Realizar exclusao de um usuario no site")
	public void excluirUsuario() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("seleciono um usuario da lista para exclus�o")
		.then_("usuario excluido da lista")
		.execute_();
	}
	
	/*
	 *  REALIZANDO A PESQUISA DE UM USUARIO EXISTENTE NO SITE 
	 */		

	@SuppressWarnings("static-access")
	@Scenario("CT007 - Realizar pesquisa de usuario existente")
	public void pesquisaUsuarioExistente() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("realizo a pesquisa de um usuario")
		.then_("sistema apresenta registro desejado")
		.execute_();
	}
	
	/*
	 *  REALIZANDO A PESQUISA DE UM NOVO USUARIO INEXISTENTE NO SITE 
	 */	
	
	@SuppressWarnings("static-access")
	@Scenario("CT008 - Realizar pesquisa de usuario inexistente")
	public void pesquisaUsuarioNaoExistente() throws ExecutionException {
		given_("acesse a pagina de SUPPLIERS")
		.when_("realizo a pesquisa de um usuario")
		.then_("sistema nao encontra usuario na busca")
		.execute_();
	}
	
}



















