package com.br.exercicio.feature.web;
import static com.br.inmetrics.frm.bdd.Gherkin.*;
import java.util.concurrent.ExecutionException;
import com.br.inmetrics.frm.bdd.Feature;
import com.br.inmetrics.frm.bdd.Scenario;

@Feature("Login")
public class LoginFeature {
	@SuppressWarnings("static-access")
	@Scenario("Acessar com usuario administrador")
    public void AcessarComUsuarioAdministrador() throws ExecutionException
    {
		given_("Dado que eu acesse o site").
		when_("Quando eu fizer o login no site").
		then_("Ent�o deverei acessar o modulo de administrador").
		execute_();
    }
	
	@SuppressWarnings("static-access")
	@Scenario("Acessar com usuario nao administrador")
    public void AcessarComUsuarioNaoAdministrador() throws ExecutionException
    {
		given_("Dado que eu acesse o site usuario comum").
		when_("Quando eu fizer o login no site").
		then_("Ent�o nao deverei acessar o modulo de admistracao").
		execute_();
    }	
	
	@SuppressWarnings("static-access")
	@Scenario("Acessar com e-mail invalido")
    public void AcessarComEmailInvalido() throws ExecutionException
    {
		given_("Dado que eu acesse o site").
		when_("Quando eu fizer o login no site").
		then_("Ent�o devera retornar como email invalido").
		execute_();
    }	

}
