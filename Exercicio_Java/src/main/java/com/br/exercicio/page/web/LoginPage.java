package com.br.exercicio.page.web;
import static com.br.inmetrics.frm.helpers.QueryHelper.getElementByXPath;
import static com.br.inmetrics.frm.base.DefaultBaseController.getDriver_;
import org.testng.Assert;
import com.br.inmetrics.frm.base.PageBase;
import com.br.inmetrics.frm.base.VirtualElement;
import com.br.inmetrics.frm.exceptions.ElementFindException;
import com.br.inmetrics.frm.exceptions.GenericException;


public class LoginPage extends PageBase {
	
	@SuppressWarnings("rawtypes")
	VirtualElement
					pageLogin		=	getElementByXPath("//h2[contains(text(), 'Login Panel')]"),
					campoLogin		=	getElementByXPath("//input[@name='email']"),
					campoSenha		=	getElementByXPath("//input[@name='password']"),
					btnLogar		=	getElementByXPath("//button[@type='submit']"),
					txtAdmin		=	getElementByXPath("//span[contains(text(), 'Super Admin')]"),
					msgEmailInvalido =  getElementByXPath("//div[@class='alert alert-danger loading wow fadeIn animated animated']");

					
	public void preencherLogin(String login) throws ElementFindException {
		waitUntilExists(campoLogin);
		campoLogin.click();
		campoLogin.sendKeys(login);
	}
	
	public void preencherSenha(String senha) throws ElementFindException {
		waitUntilExists(campoSenha);
		campoSenha.click();
		campoSenha.sendKeys(senha);
	}
	
	public void logar() throws ElementFindException {
		waitUntilExists(btnLogar);
		btnLogar.click();
	}
	
	//USUARIO COMUM
	public void redirectUsuarioComum() throws GenericException {
		getDriver_().get("https://www.phptravels.net/login");
	}
	
	public void validarUsuarioAdmin() throws ElementFindException {
		waitUntilExists(txtAdmin);
		Assert.assertTrue(elementExists(txtAdmin), "Valida��o de usuario admin");
		Assert.assertEquals(txtAdmin.getText(), "Super Admin");
	}

	public void validarPaginaLogin() {
		waitUntilExists(pageLogin);
		Assert.assertTrue(elementExists(pageLogin), "Valida��o que estou na p�gina de login");
	}
	
	public void validarEmailInvalido() {
		waitUntilExists(msgEmailInvalido);
		Assert.assertTrue(elementExists(msgEmailInvalido), "Ent�o devera retornar como email invalido");			
	}	
}
