package com.br.exercicio.page.web;
import static com.br.inmetrics.frm.helpers.DataTableHelper.getDt_;
import static com.br.inmetrics.frm.helpers.QueryHelper.getElementByXPath;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import com.br.inmetrics.frm.base.PageBase;
import com.br.inmetrics.frm.base.VirtualElement;
import com.br.inmetrics.frm.exceptions.ElementFindException;
import com.br.inmetrics.frm.exceptions.GenericException;

public class CadastroPage extends PageBase{	
	
	@SuppressWarnings("rawtypes")
	VirtualElement
	
	accounts = getElementByXPath("//*[@id=\"social-sidebar-menu\"]/li[5]/a"),
	supplier = getElementByXPath("//*[@id=\"ACCOUNTS\"]/li[2]/a"),
	adicionaSuplier = getElementByXPath("//*[@id=\"content\"]/div/form/button"),
	ckbPrimeiroRegistroLinha = getElementByXPath("(//*[@class ='iCheck-helper'])[2]"),
	btnEditarPrimeiroRegistroLinha = getElementByXPath("(//*[@title='Edit'])[1]"),
	btnDeletarPrimeiroRegistroLinha = getElementByXPath("(//*[@title='DELETE'])[1]"),
	
	txtFirstName = getElementByXPath("//input[@name='fname']"),
	txtLastName = getElementByXPath("//input[@name='lname']"),
	txtEmail = getElementByXPath("//input[@name='email']"),
	txtPassword = getElementByXPath("//input[@name='password']"),
	txtmobyleNumber = getElementByXPath("//input[@name='mobile']"),
	drpCountry = getElementByXPath("//*[@id='s2id_autogen1']/a/span[2]"),	
	selecionaCountry = getElementByXPath("//*[text() = 'Albania']"),
	txtAdicionaAddress1 = getElementByXPath("//input[@name='address1']"),
	txtName = getElementByXPath("//input[@name='itemname']"),
	ckbEmailNewletter = getElementByXPath("(//div[@class='icheckbox_square-grey'])[1]"),	
	drpClicaAssignHotels = getElementByXPath("//*[@id='s2id_autogen3']/ul"),
	selecionaAssignHotels = getElementByXPath("(//*[@class='select2-result-label'])[1]"),
	drpclicaAssignTours = getElementByXPath("//*[@id='s2id_autogen5']/ul"),
	selecionaAssignTours = getElementByXPath("(//*[@class='select2-result-label'])[4]"),
	drpClicaAssignCars = getElementByXPath("//*[@id='s2id_autogen7']/ul"),
	selecionaAssignCars = getElementByXPath("(//*[@class='select2-result-label'])[3]"),
	ckbAdd = getElementByXPath("//input[@value='addTours']//.."),
	ckbEdit = getElementByXPath("//input[@value='editTours']//.."),
	ckbRemove = getElementByXPath("//input[@value='deleteTours']//.."),
	submit = getElementByXPath("//button[@class='btn btn-primary']"),	
	
	firtNameInseridoTabela = getElementByXPath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/div[2]/table/tbody/tr[1]/td[3]"),
	lastNameInseridoTabela = getElementByXPath("//*[@id=\"content\"]/div/div[2]/div/div/div[1]/div[2]/table/tbody/tr[1]/td[4]"),
	email	= getElementByXPath("//a[@href='mailto:"+getDt_().getStringOf("EMAIL")+"']"),

	mensagemEmail = getElementByXPath("//*[@class='alert alert-danger']"),
	mensagemUsuarioInexistente = getElementByXPath("//*[@class='xcrud-row']"),
	btnSearch = getElementByXPath("//*[@class='xcrud-search-toggle btn btn-default']"),
	txtSearch = getElementByXPath("(//*[@name='phrase'])[1]"),
	drpTipoBusca = getElementByXPath("(//*[@name='column'])[1]"),
	selecionaEmail = getElementByXPath("(//*[@value='pt_accounts.accounts_email'])[1]"),
	btnGo = getElementByXPath("//*[@class='xcrud-action btn btn-primary']");
	
	
	public void clicaAccounts() throws ElementFindException{
		waitUntilExists(accounts);
		accounts.click();		
	}	
	
	
	public void clicaSupplier() throws ElementFindException{
		waitUntilExists(supplier);
		supplier.click();			
	}	

	public void adicionaSupplier() throws ElementFindException{
		waitUntilExists(adicionaSuplier);
		adicionaSuplier.click();		
	}	
	
	public void selecionaRegistro() throws ElementFindException{
		waitUntilExists(ckbPrimeiroRegistroLinha);
		ckbPrimeiroRegistroLinha.click();
	}
	
	public void editarPrimeiroRegistroLinha() throws ElementFindException{
		waitUntilExists(btnEditarPrimeiroRegistroLinha);
		btnEditarPrimeiroRegistroLinha.click();		
	}
	
	public void deletarPrimeiroRegistroLinha() throws ElementFindException, GenericException{
		waitUntilExists(ckbPrimeiroRegistroLinha);
		ckbPrimeiroRegistroLinha.click();
		btnDeletarPrimeiroRegistroLinha.click();	
		
		String alert = getController().getDriver_().switchTo().alert().getText();
		
		if (alert.equals("Are you sure you want to delete?")) {
			getController().getDriver_().switchTo().alert().accept();			
		}
	}	
	
	public void limpaCampos()throws ElementFindException{
		txtFirstName.clear();
		txtLastName.clear();
		txtEmail.clear();
	}
	
	public void digitaFirstName(String firstName) throws ElementFindException{
		waitUntilExists(txtFirstName);
		txtFirstName.click();
		txtFirstName.sendKeys(firstName);			
	}
	
	public void digitaLastName(String lastName) throws ElementFindException{
		waitUntilExists(txtLastName);
		txtLastName.click();
		txtLastName.sendKeys(lastName);			
	}
	
	public void digitaEmail(String email) throws ElementFindException{
		waitUntilExists(txtEmail);
		txtEmail.click();
		txtEmail.sendKeys(email);
	}
	
	public void digitaPassword(String password) throws ElementFindException{
		waitUntilExists(txtPassword);
		txtPassword.click();
		txtPassword.sendKeys(password);
	}
	
	public void digitaMobyleNumber(String mobyNumber) throws ElementFindException{
		waitUntilExists(txtmobyleNumber);
		txtmobyleNumber.sendKeys(mobyNumber);
	}
	
	public void clicarCountry() throws ElementFindException{
		waitUntilExists(drpCountry);
		drpCountry.click();		
	}
	
	public void selecionaCountry() throws ElementFindException{
		waitUntilExists(selecionaCountry);
		selecionaCountry.click();
	}
	
	public void adicionaAddress1(String adicionaAddress1) throws ElementFindException{
		waitUntilExists(txtAdicionaAddress1);
		txtAdicionaAddress1.sendKeys(adicionaAddress1);		
	}
	
	public void digitaName(String name) throws ElementFindException{
		waitUntilExists(txtName);
		txtName.sendKeys(name);
	}
	
	public void selecionaEmailNewletter() throws ElementFindException{
		waitUntilExists(ckbEmailNewletter);
		ckbEmailNewletter.click();
	}	
	
	
	public void clicaAssignHotels() throws ElementFindException{
		waitUntilExists(drpClicaAssignHotels);
		drpClicaAssignHotels.click();
	}
	
	public void selecionaAssignHotels() throws ElementFindException{
		waitUntilExists(selecionaAssignHotels);
		selecionaAssignHotels.click();
	}
	
	
	public void clicaAssignTours() throws ElementFindException{
		waitUntilExists(drpclicaAssignTours);
		drpclicaAssignTours.click();
	}
	

	public void selecionaAssignTours() throws ElementFindException{
		waitUntilExists(selecionaAssignTours);
		selecionaAssignTours.click();
	}
	
	public void clicaAssignCars() throws ElementFindException{
		waitUntilExists(drpClicaAssignCars);
		drpClicaAssignCars.click();
	}
	
	public void selecionaAssignCars() throws ElementFindException{
		waitUntilExists(selecionaAssignCars);
		selecionaAssignCars.click();
	}
	
	public void add() throws ElementFindException{
		waitUntilExists(ckbAdd);
		ckbAdd.click();
	}
	
	public void edit() throws ElementFindException{
		waitUntilExists(ckbEdit);
		ckbEdit.click();
	}
	
	public void remove() throws ElementFindException{
		waitUntilExists(ckbRemove);
		ckbRemove.click();
	}		
	
	public void adicionaSubmit() throws ElementFindException{
		waitUntilExists(submit);
		submit.click();		
	}
	
	
	public void informacoesAtualizadas() throws ElementFindException{			
		String firstName = getDt_().getStringOf("FIRST NAME");
		Assert.assertEquals(firtNameInseridoTabela.getText(), firstName);
		
		String lastName = getDt_().getStringOf("LAST NAME");
		Assert.assertEquals(lastNameInseridoTabela.getText(), lastName);
		
		String emailTabela = getDt_().getStringOf("EMAIL");
		Assert.assertEquals(email.getText(), emailTabela);	
	}
	
	public void verificaEmailInseridoTabela() throws ElementFindException{		
		String emailTabela = getDt_().getStringOf("EMAIL");
		Assert.assertEquals(email.getText(), emailTabela);		
	}
	
	
	public void verificaMensagemEmail() throws ElementFindException{
		waitUntilExists(mensagemEmail, 2);		
		Assert.assertEquals(mensagemEmail.getText(), "The Email field must contain a unique value.");		
	}
	
	

	public void verificaUsuarioExcluidoLista() throws ElementFindException{	
		String emailTabela = getDt_().getStringOf("EMAIL");	
		Assert.assertFalse(false, emailTabela);
		}
	
	
	
	public void realizarPesquisa(String search) throws ElementFindException{	
		waitUntilExists(btnSearch);
		btnSearch.click();	
		txtSearch.click();
		txtSearch.sendKeys(search);
		drpTipoBusca.click();
		selecionaEmail.click();
		btnGo.click();
	}
	
	public void verificaPesquisaUsuarioInexistente() throws ElementFindException{			
		waitUntilExists(mensagemUsuarioInexistente, 2);		
		Assert.assertEquals(mensagemUsuarioInexistente.getText(), "Entries not found.");	
	}
	
	public void verificaPesquisaUsuarioExistente() throws ElementFindException{			
		String campoSearchTabela = getDt_().getStringOf("SEARCH");
		Assert.assertEquals(email.getText(), campoSearchTabela);	
	}

}


