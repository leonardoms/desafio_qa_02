package com.br.exercicio.steps.web;

import static com.br.inmetrics.frm.base.DefaultBaseController.getPage_;
import static com.br.inmetrics.frm.helpers.DataTableHelper.getDt_;

import com.br.exercicio.page.web.CadastroPage;
import com.br.exercicio.page.web.LoginPage;
import com.br.inmetrics.frm.bdd.Step;

public class CadastroSteps {
	
	CadastroPage cadastroPage = getPage_(CadastroPage.class);
	LoginPage loginPage= getPage_(LoginPage.class);
	
	
	@Step("acesse a pagina de SUPPLIERS")
	public void realizoAcessoSUPPLIER() throws Exception {

		loginPage.preencherLogin(getDt_().getStringOf("USER"));
		loginPage.preencherSenha(getDt_().getStringOf("PASS"));
		loginPage.logar();
		cadastroPage.clicaAccounts();
		cadastroPage.clicaSupplier();		
	}	
	
	
	@Step("preencho as informacoes solicitadas da pagina")
	public void adicionoNovoSUPPLIERS() throws Exception {
		cadastroPage.adicionaSupplier();
		cadastroPage.digitaFirstName(getDt_().getStringOf("FIRST NAME"));
		cadastroPage.digitaLastName(getDt_().getStringOf("LAST NAME"));
		cadastroPage.digitaEmail(getDt_().getStringOf("EMAIL"));
		cadastroPage.digitaPassword(getDt_().getStringOf("PASSWORD"));
		cadastroPage.digitaMobyleNumber(getDt_().getStringOf("MOBILE NUMBER"));
		cadastroPage.clicarCountry();
		cadastroPage.selecionaCountry();
		cadastroPage.adicionaAddress1(getDt_().getStringOf("ANDRESS 1"));
		cadastroPage.digitaName(getDt_().getStringOf("NAME"));
		cadastroPage.selecionaEmailNewletter();
		cadastroPage.clicaAssignHotels();
		cadastroPage.selecionaAssignHotels();
		cadastroPage.clicaAssignTours();
		cadastroPage.selecionaAssignTours();
		cadastroPage.clicaAssignCars();
		cadastroPage.selecionaAssignCars();
		cadastroPage.add();
		cadastroPage.edit();
		cadastroPage.remove();		
		cadastroPage.adicionaSubmit();		
	}	
	
	@Step("seleciono um usuario da lista e edito suas informa��es")
	public void selecionarUsuarioEitarInformacoes() throws Exception {
		cadastroPage.selecionaRegistro();
		cadastroPage.editarPrimeiroRegistroLinha();
		cadastroPage.limpaCampos();
		cadastroPage.digitaFirstName(getDt_().getStringOf("FIRST NAME"));
		cadastroPage.digitaLastName(getDt_().getStringOf("LAST NAME"));
		cadastroPage.digitaEmail(getDt_().getStringOf("EMAIL"));
		cadastroPage.adicionaSubmit();	
	}
	
	@Step("seleciono um usuario da lista para exclus�o")
	public void realizarExclusaoUsuarioLista() throws Exception {
		cadastroPage.selecionaRegistro();
		cadastroPage.deletarPrimeiroRegistroLinha();
	}
	
	

	@Step("usuario excluido da lista")
	public void verificaExclusaoUsuarioLista() throws Exception {
		cadastroPage.verificaUsuarioExcluidoLista();;
	}
	
	
	@Step("informa��es atualizadas")
	public void verificaInformacoesAtualizadas() throws Exception {
		cadastroPage.informacoesAtualizadas();
	}
	
	
	@Step("sistema armazena o novo cadastro")
	public void sistemaDirecionaParaPaginaInicial() throws Exception {
		cadastroPage.verificaEmailInseridoTabela();
	}
	
	@Step("realizo cadastro de um email ja existente")
	public void sistemaVerificaEmailTabela() throws Exception {
		cadastroPage.verificaEmailInseridoTabela();
	}
	
	
	
		
	@Step("realizo a pesquisa de um usuario")	
	public void sistemaRealizaPesquisaUsuario() throws Exception {
		cadastroPage.realizarPesquisa(getDt_().getStringOf("SEARCH"));			
	}	
	
	@Step("sistema n�o permite inclusao de um email existente")	
	public void verificaMnsagemEmailExistente() throws Exception {
		cadastroPage.verificaMensagemEmail();
	}
	
	
	@Step("sistema nao encontra usuario na busca")	
	public void sistemaVerificaUsuarioInexistente() throws Exception {
		cadastroPage.verificaPesquisaUsuarioInexistente();
	}
	
	@Step("sistema apresenta registro desejado")	
	public void sistemaVerificaUsuarioExistente() throws Exception {
		cadastroPage.verificaPesquisaUsuarioExistente();
	}
}
	
	
	