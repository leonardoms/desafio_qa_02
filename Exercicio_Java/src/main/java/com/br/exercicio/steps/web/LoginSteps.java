package com.br.exercicio.steps.web;

import static com.br.inmetrics.frm.base.DefaultBaseController.getPage_;
import static com.br.inmetrics.frm.helpers.DataTableHelper.getDt_;

import com.br.inmetrics.frm.bdd.DesignSteps;
import com.br.inmetrics.frm.bdd.Step;
import com.br.inmetrics.frm.exceptions.ElementFindException;
import com.br.exercicio.page.web.LoginPage;

@DesignSteps
public class LoginSteps {
	
	LoginPage loginPage = getPage_(LoginPage.class);
	
	@Step("Dado que eu acesse o site")	
    public void dadoQueEuEstejaComUsuarioAdmin(){
		loginPage.validarPaginaLogin();
		
    }
	
	@Step("Quando eu fizer o login no site")	
    public void quandoFizerLoginNoSite() throws ElementFindException{
   
		loginPage.preencherLogin(getDt_().getStringOf("USER"));
		loginPage.preencherSenha(getDt_().getStringOf("PASS"));
		loginPage.logar();
		
    }
	
	@Step("Ent�o deverei acessar o modulo de administrador")	
    public void EntaoDevereiAcessarModuloAdministrativo() throws ElementFindException{
		loginPage.validarUsuarioAdmin();		
    }	
	
	
	@Step("Ent�o devera retornar como email invalido")
    public void EntaoDeveraRetornarEmailInvalido() throws ElementFindException{
		loginPage.validarEmailInvalido();		
    }
	
		
	@Step("Dado que eu acesse o site usuario comum")
	public void dadoQueEuAcesseOSiteUsuarioComum() {
		
	}
	
	

}
